package mn.fullstream.maptms;

import android.Manifest;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.osmdroid.DefaultResourceProxyImpl;
import org.osmdroid.tileprovider.tilesource.ITileSource;
import org.osmdroid.tileprovider.tilesource.XYTileSource;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    MapView map;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        map = (MapView) findViewById(R.id.myMapView);
        setUpMap();
    }


    void setUpMap(){

        GeoPoint toPoint;

        map.setMultiTouchControls(true);
        map.getController().setZoom(18);

        GPSTracker gps = new GPSTracker(MainActivity.this);

        try {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
        }catch (Exception e){
            e.printStackTrace();
        }

        if(gps.canGetLocation()){

            toPoint = new GeoPoint(gps.getLatitude(), gps.getLongitude());

            ArrayList<OverlayItem> overlayItems = new ArrayList<OverlayItem>();
            CustomOverLay overlays = new CustomOverLay(overlayItems, getResources().getDrawable(R.mipmap.ic_cpin));

            OverlayItem overlayItem = new OverlayItem("Миний байршил", gps.getLatitude() +","+gps.getLongitude(), toPoint);
            overlays.addItem(overlayItem);

            map.getOverlays().add(overlays);

        }else{

            int lat = (int) (47.8863990 * 1E6);
            int lng = (int) (106.9057440* 1E6);

            toPoint = new GeoPoint(lat, lng);

            ArrayList<OverlayItem> overlayItems = new ArrayList<OverlayItem>();
            CustomOverLay overlays = new CustomOverLay(overlayItems, getResources().getDrawable(R.mipmap.ic_cpin));

            OverlayItem overlayItem = new OverlayItem("Миний байршил", 47.8863990  +","+106.9057440, toPoint);
            overlays.addItem(overlayItem);

            map.getOverlays().add(overlays);

        }


        map.getController().animateTo(toPoint);

        //set current location Icon
        final ITileSource tileSource;
        tileSource = new XYTileSource("MyTile1", 10, 21, 128, ".png?token=iC2Xv8QX2D9OHErK2Ya56kxdsuQX1b7R", new String[]{"https://api.minu.mn/songo/"});

        map.setTileSource(tileSource);
        map.setTilesScaledToDpi(true);
        map.invalidate();



    }

    public class CustomOverLay extends ItemizedIconOverlay<OverlayItem> {
        public CustomOverLay(ArrayList<OverlayItem> overlayItems, Drawable drawable) {
            super(overlayItems, drawable, new OnItemGestureListener<OverlayItem>() {
                        @Override
                        public boolean onItemSingleTapUp(final int index, final OverlayItem item) {

                            return true;
                        }

                        @Override
                        public boolean onItemLongPress(final int index, final OverlayItem item) {

                            return false;
                        }},

                    new DefaultResourceProxyImpl(getApplicationContext()));
        }

        @Override
        protected boolean onSingleTapUpHelper(int index, OverlayItem item, MapView mapView) {

            return super.onSingleTapUpHelper(index, item, mapView);
        }
    }
}
